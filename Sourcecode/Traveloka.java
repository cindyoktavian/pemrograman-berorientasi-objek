import java.util.*;

public class Traveloka {
public static void main(String[] args) {
Scanner input = new Scanner(System.in);
String username, password;
boolean loggedIn = false;

    // Login
    System.out.println("SELAMAT DATANG DI TRAVELOKA !");
    System.out.println("Life, Your Way");
    System.out.println("Silakan login untuk melanjutkan.");
    System.out.println();
    System.out.print("Username: ");
    username = input.nextLine();
    System.out.print("Password: ");
    password = input.nextLine();
    Login login = new Login("Cio", "281002");
    if (login.authenticate(username, password)) {
        loggedIn = true;
        System.out.println();
        System.out.println("Login berhasil!");
    } else {
        System.out.println("Username atau password yang anda masukan salah.  Aplikasi akan ditutup.");
        System.exit(0);
    }
    System.out.println();

    int pil = 0;
    while(pil != 6){
        System.out.println("Silahkan Pilih Sesuai Kebutuhan Anda");
        System.out.println("1. Pembelian Tiket ");
        System.out.println("2. Reservasi Hotel");
        System.out.println("3. Layanan Mobil");
        System.out.println("4. Uang Digital");
        System.out.println("5. Tagihan");
        System.out.println("6. Logout");
        System.out.print("pilihan : "); 
        pil = input.nextInt();
    
        Tiket tiket1 = new Tiket();
        TiketKeretaApi tiket2 = new TiketKeretaApi();
        TiketBus  tiket3 = new TiketBus();
        TiketPesawat tiket4 = new TiketPesawat();
        TiketHotel tikethotel1 = new TiketHotel();
        ReservasiHotel tikethotel2 = new ReservasiHotel();
        RentalMobil rentalmobil1 = new RentalMobil();
        MenuTagihan menutagihan = new MenuTagihan();
        Uang uang1 = new Uang(200000);
        Uangku uang2 = new Uangku(500000, "Rupiah");
        System.out.println(); 
        
        switch (pil) {
            case 1:
                System.out.println("Pilih Pemesanan Tiket");
                System.out.println("1. Tiket Kereta Api");
                System.out.println("2. Tiket Bus");
                System.out.println("3. Tiket Pesawat");
                System.out.print("pilihan anda : ");
                pil = input.nextInt();
                System.out.println();

                if (pil == 1){
                    tiket2.pesan();
                } else if (pil == 2){
                    tiket3.pesan();
                } else if (pil == 3){
                    tiket4.pesan();
                } else {
                    System.out.println("input yang anda masukan salah yah coba ulangi lagi");
                }
                System.out.println();
                break;
            case 2:
                System.out.println("Informasi Hotel Wilayah Bandung");
                System.out.println("Hotel Ibis Bandung Trans Studio Harga 500000 Permalam");
                System.out.println("Hotel Grand Tjokro Premiere Bandung Harga 634000 Permalam");
                System.out.println("Hotel El Cavana Bandung Harga 550000 Permalam");
                System.out.println("Hotel Savoy Homann Harga 700000 Permalam");
                System.out.println("Reservasi hotel");
                tikethotel2.pesan();
                break;
            case 3:
                System.out.println("Rental Mobil");
                rentalmobil1.tampilDataPenyewa();
                break;
            case 4:
                System.out.println("Uangku");
                uang1.tampilkanJumlah();
                uang2.tampilkanJumlah();
                break;
            case 5:
                System.out.println("Tagihan");
                menutagihan.tampilkanTagihan();
                break;
            case 6:
                loggedIn = false;
                System.out.println("Logout berhasil!");
                break;
            default:
                System.out.println("Input yang anda masukan salah");
                break;
            }
        }
    }
}