import java.util.*;

class PLN extends Tagihan {
    private String nama;
    private String idPelanggan;
    private String alamat;
    private int jumlahTagihan;
    private int jumlahBulan;
    private int tagihanPLN;
    Scanner input = new Scanner(System.in);
    public void hitungTagihan(){
        System.out.print("Masukan nama : ");
        nama = input.nextLine();
        System.out.print("Masukan ID Pelanggan : ");
        idPelanggan = input.nextLine();
        System.out.print("Masukan alamat : ");
        alamat = input.nextLine();
        System.out.print("Masukan jumlah bulan : ");
        jumlahBulan = input.nextInt();
        System.out.print("Masukan jumlah tagihan : ");
        tagihanPLN = input.nextInt();

        jumlahTagihan = jumlahBulan * tagihanPLN;
        System.out.println("Berhasil membayar tagihan PLN sebesar Rp " + jumlahTagihan);
        System.out.println();
    }
}