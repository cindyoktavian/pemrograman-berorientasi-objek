import java.util.*;

public abstract class Tagihan {
    //method abstract
    public abstract void hitungTagihan();
}                                                                                                                           

class MenuTagihan{
    Pulsa pulsa = new Pulsa();
    BPJS bpjs = new BPJS();
    PLN pln = new PLN();
    int pilihanTagihan;
    Scanner input = new Scanner(System.in);
    public void tampilkanTagihan(){
        System.out.println("1. Pulsa Paket Data Internet");
        System.out.println("2. BPJS Kesehatan");
        System.out.println("3. PLN");
        System.out.print("Pilihan : ");
        pilihanTagihan = input.nextInt();

        switch (pilihanTagihan) {
            case 1:
                pulsa.hitungTagihan();
                break;
            case 2:
                bpjs.hitungTagihan();
                break;
            case 3:
                pln.hitungTagihan();
                break;
            default:
                System.out.println("Inputan yang anda masukan salah");
                break;
        }
    }
}