import java.util.*;

class BPJS extends Tagihan {
    private String nama;
    private int usia, tagihanBPJS, jumlahTagihan;
    Scanner input = new Scanner(System.in);
    public void hitungTagihan(){
        System.out.println();
        System.out.print("Masukan nama : ");
        nama = input.nextLine();
        System.out.print("Masukan jumlah tagihan : ");
        tagihanBPJS = input.nextInt();
        System.out.print("Masukan usia : ");
        usia = input.nextInt();

        if(usia >= 20 && usia <= 35){
            jumlahTagihan = tagihanBPJS / 20;  
            System.out.println("Berhasil membayar tagihan BPJS sebesar Rp " + jumlahTagihan);
        }else if(usia > 35  && usia >= 70){
            jumlahTagihan = tagihanBPJS / 10;  
            System.out.println("Berhasil membayar tagihan BPJS sebesar Rp " + jumlahTagihan);
        }else{
            System.out.println("Inputan yang anda masukan salah");
        }
        System.out.println();
    }
}