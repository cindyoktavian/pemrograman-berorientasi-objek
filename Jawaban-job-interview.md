# no.1
Program ini menggunakan algoritma pemrograman terstruktur dengan pendekatan berbasis objek (Object-Oriented Programming). Algoritma yang digunakan dalam program ini adalah algoritma kontrol alur berbasis pengulangan (loop) dan pemilihan (switch-case) untuk mengatur alur program sesuai dengan pilihan yang diinputkan oleh pengguna.

Berikut adalah alur kerja program dalam algoritma yang digunakan:

1. Membaca input username dan password dari pengguna.
2. Melakukan proses autentikasi login dengan memanggil metode `authenticate` dari objek `Login`.
3. Jika autentikasi berhasil, variabel `loggedIn` akan diubah menjadi `true` dan program akan melanjutkan ke langkah berikutnya. Jika gagal, program akan menampilkan pesan kesalahan dan keluar dari program.
4. Menampilkan menu pilihan kepada pengguna menggunakan pernyataan `switch-case` dalam loop `while`.
5. Membaca input pilihan dari pengguna.
6. Berdasarkan pilihan yang diinputkan, program akan melakukan tindakan yang sesuai dengan pilihan tersebut dengan memanggil metode dari objek yang sesuai.
7. Setelah tindakan selesai dilakukan, program akan kembali ke langkah 4 dan menampilkan menu pilihan kembali kepada pengguna.
8. Jika pengguna memilih opsi "Logout" (pilihan 6), variabel `loggedIn` akan diubah menjadi `false` dan program akan keluar dari loop `while`.
9. Jika pengguna memilih opsi lain, program akan terus berjalan hingga pengguna memilih opsi "Logout" dan keluar dari program.

**Pseudocode**


Judul: Traveloka

 Deklarasi: method main()

        Declare input as Scanner
        Declare loggedIn as Boolean
        Declare username, password as String
        Declare pil as Integer
        Declare tiket1, tiket2, tiket3, tiket4, tikethotel1, tikethotel2, rentalmobil1 as Objects

        Declare menutagihan, uang1, uang2 as Objects

Implementasi:
        
        print "SELAMAT DATANG DI TRAVELOKA !"
        print "Life, Your Way"
        print "Silakan login untuk melanjutkan."
        print ""

        print "Username: "
        username = input.nextLine()
        print "Password: "
        password = input.nextLine()

        login = new Login("Cio", "281002")
        if login.authenticate(username, password)
            loggedIn = true
            print ""
            print "Login berhasil!"
        else
            print "Username atau password yang anda masukan salah. Aplikasi akan ditutup."
            exit(0)

        print ""

        while pil != 6
            print "Silahkan Pilih Sesuai Kebutuhan Anda"
            print "1. Pembelian Tiket"
            print "2. Reservasi Hotel"
            print "3. Layanan Mobil"
            print "4. Uang Digital"
            print "5. Tagihan"
            print "6. Logout"
            print "pilihan : "
            pil = input.nextInt()

            tiket1 = new Tiket()
            tiket2 = new TiketKeretaApi()
            tiket3 = new TiketBus()
            tiket4 = new TiketPesawat()
            tikethotel1 = new TiketHotel()
            tikethotel2 = new ReservasiHotel()
            rentalmobil1 = new RentalMobil()
            menutagihan = new MenuTagihan()
            uang1 = new Uang(200000)
            uang2 = new Uangku(500000, "Rupiah")

            print ""

            switch pil
                case 1
                    print "Pilih Pemesanan Tiket"
                    print "1. Tiket Kereta Api"
                    print "2. Tiket Bus"
                    print "3. Tiket Pesawat"
                    print "pilihan anda : "
                    pil = input.nextInt()
                    print ""

                    if pil == 1
                        tiket2.pesan()
                    else if pil == 2
                        tiket3.pesan()
                    else if pil == 3
                        tiket4.pesan()
                    else
                        print "input yang anda masukan salah yah coba ulangi lagi"

                    print ""
                    break

                case 2
                    print "Informasi Hotel Wilayah Bandung"
                    print "Hotel Ibis Bandung Trans Studio Harga 500000 Permalam"
                    print "Hotel Grand Tjokro Premiere Bandung Harga 634000 Permalam"
                    print "Hotel El Cavana Bandung Harga 550000 Permalam"
                    print "Hotel Savoy Homann Harga 700000 Permalam"
                    print "Reservasi hotel"
                    tikethotel2.pesan()
                    break

                case 3
                    print "Rental Mobil"
                    rentalmobil1.tampilDataPenyewa()
                    break

                case 4
                    print "Uangku"
                    uang1.tampilkanJumlah()
                    uang2.tampilkanJumlah()
                    break

                case 5
                    print "Tagihan"
                    menutagihan.tampilkanTagihan()
                    break

                case 6
                    loggedIn = false
                    print "Logout berhasil!"
                    break

                default
                    print "Input yang anda masukan salah"
                    break

            print ""
        end while
    end method
end class

Berikut implementasi programnya : [Class Traveloka](Sourcecode/Traveloka.java)


# no.2 
Program ini menggunakan algoritma pemrograman terstruktur dengan pendekatan berbasis objek (Object-Oriented Programming). Algoritma yang digunakan dalam program ini adalah algoritma kontrol alur berbasis pengulangan (loop) dan pemilihan (switch-case) untuk mengatur alur program sesuai dengan pilihan yang diinputkan oleh pengguna.

Berikut adalah alur kerja program dalam algoritma yang digunakan:

1. Membaca input username dan password dari pengguna.
2. Melakukan proses autentikasi login dengan memanggil metode `authenticate` dari objek `Login`.
3. Jika autentikasi berhasil, variabel `loggedIn` akan diubah menjadi `true` dan program akan melanjutkan ke langkah berikutnya. Jika gagal, program akan menampilkan pesan kesalahan dan keluar dari program.
4. Menampilkan menu pilihan kepada pengguna menggunakan pernyataan `switch-case` dalam loop `while`.
5. Membaca input pilihan dari pengguna.
6. Berdasarkan pilihan yang diinputkan, program akan melakukan tindakan yang sesuai dengan pilihan tersebut dengan memanggil metode dari objek yang sesuai.
7. Setelah tindakan selesai dilakukan, program akan kembali ke langkah 4 dan menampilkan menu pilihan kembali kepada pengguna.
8. Jika pengguna memilih opsi "Logout" (pilihan 6), variabel `loggedIn` akan diubah menjadi `false` dan program akan keluar dari loop `while`.
9. Jika pengguna memilih opsi lain, program akan terus berjalan hingga pengguna memilih opsi "Logout" dan keluar dari program.

**Pseudocode**


Judul: Traveloka

 Deklarasi: method main()

        Declare input as Scanner
        Declare loggedIn as Boolean
        Declare username, password as String
        Declare pil as Integer
        Declare tiket1, tiket2, tiket3, tiket4, tikethotel1, tikethotel2, rentalmobil1 as Objects

        Declare menutagihan, uang1, uang2 as Objects

Implementasi:
```
        print "SELAMAT DATANG DI TRAVELOKA !"
        print "Life, Your Way"
        print "Silakan login untuk melanjutkan."
        print ""

        print "Username: "
        username = input.nextLine()
        print "Password: "
        password = input.nextLine()

        login = new Login("Cio", "281002")
        if login.authenticate(username, password)
            loggedIn = true
            print ""
            print "Login berhasil!"
        else
            print "Username atau password yang anda masukan salah. Aplikasi akan ditutup."
            exit(0)

        print ""

        while pil != 6
            print "Silahkan Pilih Sesuai Kebutuhan Anda"
            print "1. Pembelian Tiket"
            print "2. Reservasi Hotel"
            print "3. Layanan Mobil"
            print "4. Uang Digital"
            print "5. Tagihan"
            print "6. Logout"
            print "pilihan : "
            pil = input.nextInt()

            tiket1 = new Tiket()
            tiket2 = new TiketKeretaApi()
            tiket3 = new TiketBus()
            tiket4 = new TiketPesawat()
            tikethotel1 = new TiketHotel()
            tikethotel2 = new ReservasiHotel()
            rentalmobil1 = new RentalMobil()
            menutagihan = new MenuTagihan()
            uang1 = new Uang(200000)
            uang2 = new Uangku(500000, "Rupiah")

            print ""

            switch pil
                case 1
                    print "Pilih Pemesanan Tiket"
                    print "1. Tiket Kereta Api"
                    print "2. Tiket Bus"
                    print "3. Tiket Pesawat"
                    print "pilihan anda : "
                    pil = input.nextInt()
                    print ""

                    if pil == 1
                        tiket2.pesan()
                    else if pil == 2
                        tiket3.pesan()
                    else if pil == 3
                        tiket4.pesan()
                    else
                        print "input yang anda masukan salah yah coba ulangi lagi"

                    print ""
                    break

                case 2
                    print "Informasi Hotel Wilayah Bandung"
                    print "Hotel Ibis Bandung Trans Studio Harga 500000 Permalam"
                    print "Hotel Grand Tjokro Premiere Bandung Harga 634000 Permalam"
                    print "Hotel El Cavana Bandung Harga 550000 Permalam"
                    print "Hotel Savoy Homann Harga 700000 Permalam"
                    print "Reservasi hotel"
                    tikethotel2.pesan()
                    break

                case 3
                    print "Rental Mobil"
                    rentalmobil1.tampilDataPenyewa()
                    break

                case 4
                    print "Uangku"
                    uang1.tampilkanJumlah()
                    uang2.tampilkanJumlah()
                    break

                case 5
                    print "Tagihan"
                    menutagihan.tampilkanTagihan()
                    break

                case 6
                    loggedIn = false
                    print "Logout berhasil!"
                    break

                default
                    print "Input yang anda masukan salah"
                    break

            print ""
        end while
    end method
end class
```
Berikut implementasi programnya : [Class Traveloka](Sourcecode/Traveloka.java)

# no.3
Konsep OOP

- Object adalah suatu cara untuk membungkus data dan fungsi bersama menjadi satu unit dalam sebuah program.
- Atribut merupakan identitas data atau informasi dari object yang disebut juga sebagai variable.
- Method adalah tingkah laku yang dilakukan object didalam program.
- Class merupakan blueprint (rancangan) atau prorotipe dari sebuah objek. Class akan mendefinisikan sebuah tipe dari object dimana dapat mendeklarasikan variable dan menciptakan object.

Konsep 4 pilar OOP adalah empat konsep dasar dalam Pemrograman Berorientasi Objek (OOP) yang menjadi dasar bagi pemrograman berorientasi objek.
- Encapsulation : Konsep pengkapsulan memungkinkan kita untuk menyembunyikan detail implementasi dari objek dan hanya menampilkan fungsionalitas yang dapat diakses oleh pengguna. Hal ini dilakukan dengan membatasi akses ke properti dan metode objek dengan menggunakan access modifiers seperti public, private, dan protected. Encapsulation merupakan proses di mana sebuah penanganan data ditempatkan di dalam wadah tunggal yang disebut sebagai class. Dengan begini, kita cukup menggunakan data tersebut tanpa harus tahu bagaimana proses yang terjadi sampai data tersebut bisa digunakan. Salah satu contoh dalam program yang menggunakan konsep encapsulation yaitu **class Login**.

- Inheritance (Pewarisan) : Konsep pewarisan memungkinkan kita untuk membuat kelas baru dengan memanfaatkan sifat-sifat yang sudah didefinisikan dalam kelas yang sudah ada. Dengan cara ini, kita dapat menghemat waktu dalam penulisan kode dan menghindari duplikasi kode. Contoh dalam program yaitu class **TiketKeretaApi**, class **TiketPesawat**, class **TiketBus** merupakan subclass dari class **Tiket** sebagai superclassnya.

- Polymorphism (Banyak Bentuk) Konsep polimorfisme memungkinkan kita untuk menggunakan metode yang sama pada objek yang berbeda-beda. Dalam hal ini, satu metode bisa memiliki implementasi yang berbeda-beda pada setiap objek, tergantung pada kebutuhan yang ada. Contoh dalam program yaitu class **Uang** dan class **Uangku** 

- Abstraction : Konsep abstraksi memungkinkan kita untuk membuat objek yang lebih kompleks dengan cara membaginya menjadi objek yang lebih sederhana dan lebih mudah dimengerti. Hal ini dilakukan dengan cara menyederhanakan dan mengkapsulkan fitur objek sehingga kemudian dapat diakses atau digunakan dengan lebih mudah. Contoh dalam program yaitu class **Tagihan** yang dimana class **Tagihan** sebagai superclassnya kemudian class **BPJS**, class **PLN**, dan class **Pulsa** sebagai subclass.

Keempat konsep ini bersama-sama membentuk dasar dari pemrograman berorientasi objek dan menjadi landasan dalam membuat aplikasi yang lebih kompleks.

# no.4
Enkapsulasi adalah salah satu prinsip dasar dalam pemrograman berorientasi objek (OOP). Enkapsulasi memungkinkan untuk memproteksi variabel dan informasi lainnya dalam sebuah objek agar tidak dapat diakses atau diubah oleh objek lain. Ini dilakukan dengan menggunakan konsep aksesibilitas, seperti memberi akses publik, protektif, atau privat pada variabel dan metode di dalam objek. Sehingga, hanya objek itu sendiri yang dapat mengakses dan memodifikasi variabel dan informasi dalam objek.
Oleh karena itu, enkapsulasi adalah penting dalam program OOP karena memastikan bahwa fungsi dan variabel hanya dapat diakses dan dimodifikasi dengan cara yang lebih terstruktur dan aman. Ini juga memudahkan pengembangan objek baru di dalam program dan memungkinkan untuk menghindari masalah yang 
mungkin terjadi akibat variabel dan informasi yang tidak terkontrol oleh objek lainnya. [Class Login](Sourcecode/Login.java)

# no.5
Abstraction diibaratkan class abstrak merupakan sebuah wadah dan turunan dari class abstrak merupakan isinya atribut dan method yang ada pada abstrak harus ada di class turunan/sub class. Abstraction juga sangat berguna untuk membuat kode yang lebih mudah dimengerti, dikelola, dan diperbaiki. Kita dapat fokus pada fitur yang dibutuhkan, tanpa terlalu terjebak pada implementasi detail. Sebagai contoh, dalam pengembangan aplikasi, kita dapat menentukan spesifikasi data yang dibutuhkan dengan menggunakan interface, dan kemudian menerapkan class sesuai dengan spesifikasi tersebut. [Class Tagihan](Sourcecode/Tagihan.java)


# no.6
Polymorphism (Banyak Bentuk) Konsep polimorfisme memungkinkan kita untuk menggunakan metode yang sama pada objek yang berbeda-beda. Dalam hal ini, satu metode bisa memiliki implementasi yang berbeda-beda pada setiap objek, tergantung pada kebutuhan yang ada.Dibawah ini merupakan pengimplementasian Polymorphism
[Uang.java](/uploads/fa605b5c964da4be5e98d7ea2153d94c/Uang.java)
[Uangku.java](/uploads/3ab1c6d281962f2c9794c2c86e598aa3/Uangku.java)

Inheritance
Konsep inheritance dalam program dibawah adalah TiketKeretaApi, TiketBus, TiketPesawat sebagai subclass yang mewarisi sifat dan perilaku dari superclass Tiket. Subclass TiketKeretaApi, TiketBus, TiketPesawat menggunakan variabel dan method yang telah didefinisikan di superclass Tiket, seperti variabel nama, tanggal_berangkat, awal, tujuan, jumlah_tiket, jenis_kereta, harga_tiket, total_harga dan method pesan. Dengan menggunakan inheritance, subclass TiketKeretaApi, TiketBus, TiketPesawat dapat memperluas fungsionalitas dari superclass Tiket dan membantu memudahkan pengembangan program secara modular. Dibawah ini merupakan pengimplementasian Inheritance yang dimana  menggunakan inheritance lebih efisien dan menghindari duplikasi code/codingan
[Tiket.java](/uploads/7e1ef91bac3351468fff2c22b2dc2d2e/Tiket.java)
[TiketKeretaApi.java](/uploads/46afc4a9140c771dd5cf450837c30b7d/TiketKeretaApi.java)
[TiketPesawat.java](/uploads/29a4148659c259fc71e9d4b5125b3483/TiketPesawat.java)
[TiketBus.java](/uploads/653f67f90a84c52be1bee2dbcad5f239/TiketBus.java)

# no.7
1. Use case Login : untuk mengotentikasi pengguna agar dapat melakukan login pada aplikasi. Diimplementasikan pada class Login
2. Use case Memesan Tiket : untuk merepresentasikan tiket yang dibeli oleh pengguna. Diimplementasikan pada class Tiket,Tiket Pesawat, Bus dan Kereta Api.
3. Use case reservasi hotel : untuk merepresentasikan proses reservasi hotel. Diimplementasikan pada class TiketHotal dan class Resevasi hotel
4. Use Case Memesanan mobil : untuk merepresentasikan proses rental mobil. Diimplementasikan pada class RentalMobil
7. Use Case Cek Saldo : untuk merepresentasikan jumlah uang pada akun pengguna dan merepresentasikan dompet digital pengguna. Diimplementasikan pada class Uang dan class Uangku
9. Use Case Membayar Tagihan: untuk menampilkan informasi tagihan yang harus dibayar oleh pengguna. Diimplementasikan pada class Tagihan, class BPJS, class Pulsa, dan Class PLN.

# no.8
 Class Diagram
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Class_Diagram.drawio.png)
Use Case 
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/canvas.png)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/canvas1.png)

# no.9
Link yt : https://youtu.be/wj7dSMFSUkY

# no.10 
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image1.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image2.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image3.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image5.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image4.jpg)













