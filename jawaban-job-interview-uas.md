# no.1 
Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/classdiagramuas4.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/classdiagramuas1.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/classdiagramuas3.jpg)

# no.2
Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Class_Diagram.drawio.png)


# no.3
Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

1. Single Responsibility Principle (SRP):
Setiap kelas memiliki tanggung jawab tunggal yang terdefinisi dengan jelas. Misalnya, kelas home bertanggung jawab atas tindakan pengguna. 
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/srpl.jpg)

2. Open/Closed Principle (OCP):
Pada contoh kode di atas, memisahkan fungsi yang bertanggung jawab untuk membuat konten HTML halaman dari fungsi yang menangani rute (route handler) di dalam file routes.php.
Hal ini mengikuti Prinsip Pembukaan Tertutup (Open/Closed Principle atau OCP) dengan membuat kode lebih mudah diperluas tanpa mengubah fungsi rute utama (homeHandler).
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/OCP.jpg)

3. Liskov Substitution Principle (LSP):

4. Interface Segregation Principle (ISP):
Tidak ada penerapan langsung dari Prinsip Segregation Antarmuka (Interface Segregation Principle atau ISP) dalam kode di atas. Prinsip ISP berbicara tentang membagi antarmuka yang besar menjadi beberapa antarmuka yang lebih kecil dan lebih spesifik, sehingga  hanya perlu mengimplementasikan metode yang relevan.
Pada kode tersebut,  hanya memiliki satu kelas dasar Page yang memiliki satu metode createHTMLContent(). Tidak ada pembagian atau segregasi antarmuka lebih lanjut dalam bentuk antarmuka terpisah. Oleh karena itu, kode tersebut tidak menerapkan Prinsip ISP.

5. Dependency Inversion Principle (DIP):

# no.4
Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

Menerapkan Pola Singleton untuk BookingManager yang mengelola operasi pemesanan. Fungsi createInstance() menginisialisasi objek BookingManager dengan properti dan metodenya, dan hanya dipanggil sekali untuk memastikan ada satu instance. Fungsi getInstance() memastikan bahwa hanya satu instance BookingManager yang dibuat dan dikembalikan saat diakses.

<!DOCTYPE html>
<html>
<head>
    <!-- Head content here -->
</head>
<body>
    <div class="container">
        <h2>Pemesanan Tiket</h2>
        <!-- Other content and forms here -->
    </div>

    <!-- JavaScript libraries -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script>
        // Singleton Pattern implementation for the BookingManager
        const BookingManager = (function() {
            let instance;

            function createInstance() {
                // Your logic for initializing the BookingManager instance here
                // For example, you can manage the booking data and methods here

                return {
                    bookTicket: function(ticketDetails) {
                        // Your booking logic here
                        console.log("Booking the ticket:", ticketDetails);
                    },
                    // Other methods related to the booking process
                };
            }

            return {
                getInstance: function() {
                    if (!instance) {
                        instance = createInstance();
                    }
                    return instance;
                },
            };
        })();

        // Usage of the BookingManager Singleton
        const bookingManager = BookingManager.getInstance();

        // Example of using the BookingManager to book a ticket
        bookingManager.bookTicket({
            ticketType: "hotel",
            hotelName: "Hotel ABC",
            checkInDate: "2023-07-15",
            checkOutDate: "2023-07-20",
            numberOfGuests: 2,
            numberOfRooms: 1
        });
    </script>
</body>
</html>


# no.5
Mampu menunjukkan dan menjelaskan konektivitas ke database

![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/UASPBO1.jpg)

Berfungsi untuk melakukan otentikasi pengguna dengan memeriksa username dan password. Dengan data yang ada di tabel "user" dalam database.

# no.6
Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
- Web page untuk user 
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/tampilangui.jpg)

- Web service 
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/webserive.jpg)

CRUD disini berfungi untuk mengedit data pemenasan tiket dan menghapus pemesanan tiket. Salah satunya terdapat di booking hotel : 

    <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $nama = $_POST["nama"];
                    $hotel = $_POST["hotel"];
                    $tanggal = $_POST["tanggal"];
                    $tamu = $_POST["tamu"];
                    $kamar = $_POST["kamar"];

                    echo "<p>Nama: " . $nama . "</p>";
                    echo "<p>Hotel: " . $hotel . "</p>";
                    echo "<p>Tanggal: " . $tanggal . "</p>";
                    echo "<p>Tamu: " . $tamu . "</p>";
                    echo "<p>Kamar: " . $kamar . "</p>";
                }
                ?>
                <a href="edit_booking.php" class="btn btn-primary">Edit</a>
                <a href="#" onclick="deleteBooking()" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
    <a href="index.php">Back</a>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
        function deleteBooking() {
            if (confirm("Are you sure you want to delete this booking?")) {
                // Perform delete operation here (you can use Ajax to communicate with the server)
                alert("Booking deleted successfully!");
            }
        }
    </script>

![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/CRUD.jpg)

# no.7
Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/tampilangui.jpg)

Berikut adalah penjelasan mengenai antarmuka pengguna (Graphical User Interface/GUI) dari produk digital traveloka:

1. Header:
    - Pada bagian atas halaman, terdapat header dengan latar belakang warna abu-abu (#F2F2F2) dan padding 20px.
    - Di dalam header, terdapat judul "Welcome to Traveloka" dengan ukuran font 32px berwarna hitam (#333333).

2. Hero Section:
    - Bagian ini merupakan bagian hero dengan latar belakang gambar (1.jpg) yang ditampilkan dengan ukuran cover dan posisi di tengah (background-size: cover; background-position: center).
    - Di dalam hero section, terdapat teks "Discover and Book Amazing Travel Experiences" dengan ukuran font 48px berwarna putih (#FFFFFF) dan berada di tengah layar.

3. CTA Button:
    - Di bawah teks hero, terdapat tombol "Pesan" (cta-button) berwarna oranye (#FF6600) dengan teks putih (#FFFFFF).
    - Tombol tersebut memiliki ukuran font 24px, padding 10px atas dan bawah, serta 30px kiri dan kanan. Tombol tersebut tidak memiliki border (border: none) dan diberi sudut melengkung (border-radius: 4px).
    - Ketika kursor diarahkan ke tombol, warna latar belakang berubah menjadi oranye lebih gelap (#FF5500) untuk memberikan efek hover.

4. Quote Section:
    - Bagian ini berisi kutipan (quote) dengan latar belakang warna abu-abu (#F2F2F2) dan padding 20px. Bagian ini memiliki sudut melengkung (border-radius: 4px).
    - Di dalam quote section, terdapat teks kutipan "The world is a book and those who do not travel read only one page." dengan ukuran font 24px dan gaya tulisan miring (italic).
   - Quote section ini memiliki teks yang berada di tengah halaman.

- Halaman Pemesanan Tiket:
    - Halaman ini adalah halaman utama dari aplikasi pemesanan tiket.
    - Terdapat judul "Pemesanan Tiket" di bagian atas halaman.
     - Terdapat empat tab navigasi: "Tiket Hotel" (default aktif), "Tiket Pesawat", "Tiket Kereta Api", dan "Log Out" (tautan untuk keluar dari aplikasi atau fungsi logout. 
     - Pengguna dapat mengklik tab "Tiket Hotel," "Tiket Pesawat," atau "Tiket Kereta Api" untuk beralih antara jenis tiket yang berbeda.

- Tab Tiket Hotel
    - Tab "Tiket Hotel" adalah tab aktif default ketika halaman dimuat.
    - Pengguna dapat mengisi formulir untuk memesan tiket hotel. Formulir ini mencakup kolom untuk Nama, Pilihan Hotel, Tanggal Check-in dan Check-out, Jumlah Tamu, dan Jumlah Kamar.
    - Setelah mengisi detail tersebut, pengguna dapat klik tombol "Pesan Tiket Hotel" untuk mengirimkan permintaan pemesanan.
      - Di tampilan setelah pesan lalu ada edit data pemesanan dan delete untuk menghapus pemesanan

- Tab Tiket Pesawat 
    - Untuk mengakses tab "Tiket Pesawat", pengguna dapat mengklik tautan "Tiket Pesawat" di navigasi.
    - Pengguna dapat mengisi formulir untuk memesan tiket pesawat. Formulir ini mencakup kolom untuk Nama, Pilihan Bandara Keberangkatan, Pilihan Bandara Tujuan, Kelas Penerbangan, dan Tanggal keberangkatan.
    - Setelah mengisi detail tersebut, pengguna dapat klik tombol "Pesan Tiket Pesawat" untuk mengirimkan permintaan pemesanan.
      - Di tampilan setelah pesan lalu ada edit data pemesanan dan delete untuk menghapus pemesanan

- Tab Tiket Kereta Api
   - Untuk mengakses tab "Tiket Kereta Api", pengguna dapat mengklik tautan "Tiket Kereta Api" di navigasi.
    - Pengguna dapat mengisi formulir untuk memesan tiket kereta api. Formulir ini mencakup kolom untuk Nama, Pilihan Stasiun Keberangkatan, Pilihan Stasiun Tujuan, Kelas, dan Tanggal perjalanan kereta.
    - Setelah mengisi detail tersebut, pengguna dapat klik tombol "Pesan Tiket Kereta Api" untuk mengirimkan permintaan pemesanan.
    - Di tampilan setelah pesan lalu ada edit data pemesanan dan delete untuk menghapus pemesanan

# no.8
Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

Terdapat dua bagian: bagian pertama halaman HTML sederhana dengan pesan selamat datang dan beberapa gaya CSS, dan bagian kedua  halaman untuk memesan tiket dengan tab untuk jenis tiket yang berbeda (hotel, penerbangan, dan kereta).  bagaimana kode PHP  berinteraksi dengan GUI (Antarmuka Pengguna Grafis) dan membangun koneksi HTTP :
1. Koneksi HTTP:
HTTP adalah singkatan dari Hypertext Transfer Protocol, yang merupakan protokol yang digunakan di World Wide Web untuk memungkinkan komunikasi antara klien (seperti browser web) dan server. 

    http://localhost/Traveloka/login.php

2. GUI dalam Produk Digital:
GUI (Antarmuka Pengguna Grafis) untuk sebuah situs web pemesanan perjalanan. Menggunakan elemen HTML, CSS, dan framework Bootstrap untuk menciptakan desain yang menarik secara visual dan responsif. GUI memungkinkan pengguna berinteraksi dengan situs web, melihat bagian-bagian yang berbeda, dan mengisi formulir untuk memesan tiket hotel, penerbangan, dan kereta.
3. Bagaimana GUI Bekerja:
GUI dibuat menggunakan elemen HTML yang menentukan struktur halaman, CSS untuk mendesain elemen, dan Bootstrap untuk meningkatkan tata letak dan responsivitas. Ketika pengguna mengakses skrip PHP ini melalui server web, server akan menafsirkan kode PHP dan menghasilkan keluaran HTML yang sesuai, yang akan dikirim ke browser web pengguna.
GUI ini dibagi menjadi dua bagian utama:
- **Halaman Selamat Datang**: Bagian pertama menghasilkan halaman selamat datang dengan header, bagian hero, tombol panggilan tindakan, dan kutipan. Bagian hero memiliki gambar latar belakang dan judul. Tombol panggilan tindakan memungkinkan pengguna untuk mengambil tindakan tertentu (misalnya, memesan tiket) ketika diklik.
- **Halaman Pemesanan**: Bagian kedua menghasilkan halaman pemesanan dengan tab (menggunakan komponen tab Bootstrap) untuk berbagai jenis tiket: hotel, penerbangan, dan kereta. Setiap tab berisi formulir dengan bidang relevan (misalnya, nama, pilihan hotel, tanggal check-in/check-out, dll.) dan tombol "Pesan". Pengguna dapat mengisi formulir, memilih preferensi mereka, dan mengirimkan formulir untuk memesan tiket.

Ketika pengguna berinteraksi dengan GUI, misalnya dengan mengklik tombol "Pesan" pada formulir pemesanan, data formulir akan dikirimkan ke server melalui permintaan POST HTTP.

# no.9
Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

https://youtu.be/BeaP8i1CVpU

# no.10 
Mendemonstrasikan penggunaan Machine Learning
